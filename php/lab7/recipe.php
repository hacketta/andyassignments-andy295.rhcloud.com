<?php
require_once "conn.php";
require_once "model.php";

class Recipe
{
    public $id;
    public $title;
    public $instructions;
    
    function __construct($id, $title, $instructions)
    {
        $this->id = $id;
        $this->title = $title;
        //$this->ingrediants =$ingrediants;
        $this->instructions = $instructions;
    }
}
////////////////////////////////////////////////////////////////////////////    
class RecipeModel extends Model
{    
    function find_all()
    {
          
        $results = array();
        
        $conn = Database::get_connection();
        $query = "SELECT * from recipetable";
    
        $res = $conn->query($query);
 
        while ($row = $res->fetch_assoc())
        {
            /*
            $query1 = "SELECT * FROM ingredianttable AS ingt
                       JOIN joiningtable AS jt ON jt.ingrediantid = ingt.ingrediantid
                        WHERE jt.recipeid =".$row{'recipeid'};
           
            $res1 = $conn->query($query1);
            $ingrediants = array();
            while ($row1 = $res1->fetch_assoc())
            {
                $ingrediants[] = $row1{'ingrediants'};
                
            }*/
            $results[] = new Recipe(
                $row['recipeid'],
                $row['title'],
                $row['instructions']
                );
        }              
        return $results;
    
    }

    function insertRecipe()
    {
        
        $conn = Database::get_connection();
        //$title = htmlentities($_POST['title']);
        //$instructions = htmlentities($_POST['instructions']);
        $query = $conn->prepare("INSERT INTO recipetable (title, instructions) VALUES (?, ?)");      
        $query->bind_param('ss', $_POST['title'], $_POST['instructions']);
        $query->execute();
             
        foreach(query1 as $query1)
        {
            $query1 = $conn->prepare("INSERT INTO ingredianttable (ingrediants) VALUES (?)"); 
        $query1->bind_param('s', $_POST['ingrediants']);
        $query1->execute();    
        }
        
                
    }
         

      
        /* public $ingrediantsid = array();
        $conn = get_connection();
         $recipeid = htmlentities($_POST['recipeid']);
        $ingrediantid = htmlentities($_POST['ingrediantid']);
        $query2 = $conn->prepare("INSERT INTO joiningtable (recipeid, ingrediantid) VALUES (?, ?)");
        $query2->bind_param('ss', $recipeid, $ingrediants);
      $query1->execute();
        */

    function removeRecipe($recipeid)
    {
	$conn = Database::get_connection();
	$query = $conn->prepare("DELETE FROM recipetable WHERE recipeid = ?");
	$query->bind_param('i', $recipeid);
        $query->execute();
		 
    }
}
	
?>
