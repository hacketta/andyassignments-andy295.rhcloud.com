-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2013 at 07:46 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lab7`
--

-- --------------------------------------------------------

--
-- Table structure for table `ingredianttable`
--

CREATE TABLE IF NOT EXISTS `ingredianttable` (
  `ingrediantid` int(11) NOT NULL AUTO_INCREMENT,
  `ingrediants` varchar(128) NOT NULL,
  PRIMARY KEY (`ingrediantid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='stores ingrediants' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ingredianttable`
--

INSERT INTO `ingredianttable` (`ingrediantid`, `ingrediants`) VALUES
(1, 'Tomatoes'),
(2, 'Garlic');

-- --------------------------------------------------------

--
-- Table structure for table `joiningtable`
--

CREATE TABLE IF NOT EXISTS `joiningtable` (
  `recipeid` int(11) NOT NULL,
  `ingrediantid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='joins recipe and ingrediant table';

--
-- Dumping data for table `joiningtable`
--

INSERT INTO `joiningtable` (`recipeid`, `ingrediantid`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `recipetable`
--

CREATE TABLE IF NOT EXISTS `recipetable` (
  `recipeid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `instructions` varchar(128) NOT NULL,
  PRIMARY KEY (`recipeid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='recipetable' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `recipetable`
--

INSERT INTO `recipetable` (`recipeid`, `title`, `instructions`) VALUES
(1, 'Spaghetti', 'Make it Good'),
(2, 'Pad Thai', 'Make it Spicey');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
