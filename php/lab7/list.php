         
<a href="index.php?action=insert">Insert a recipe</a>

<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>title</th>                  
            <th>ingrediants</th>
            <th>instructions</th>
            <th>Delete Recipe</th>
        </tr>
        
    </thead>
    <tbody>
    <?php foreach ($this->data as $recipe) { ?>     
        <tr>
            <td><?php echo htmlentities($recipe->id); ?></td>
            <td><?php echo htmlentities($recipe->title); ?></td>
            <td>
            </td>
            <td><?php echo htmlentities($recipe->instructions); ?></td>
            <td><a href="index.php?remove=<?php echo $recipe->id ?>">Delete</a></td>
        </tr>                                
   <?php  } ?>          
    </tbody>                
</table> 