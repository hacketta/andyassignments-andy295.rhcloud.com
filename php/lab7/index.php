        
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


require_once "conn.php";
require_once "recipe.php";
require_once "view.php";

    if($_SERVER['REQUEST_METHOD'] == 'GET' && (isset($_GET['action']) && $_GET['action'] == 'insert'))
    {
	$pageName = "insert";
	$pageData = NULL;
	$viewObject = new View($pageName, $pageData);
	$viewObject->Render();
   
    }
    elseif($_SERVER['REQUEST_METHOD'] == 'POST' && (isset($_GET['action']) && $_GET['action'] == 'insert'))
    {
	$pageName = "thanks";
	$pageData = new RecipeModel();
	$pageData->insertRecipe();
	$viewObject = new View($pageName, $pageData);
	$viewObject->Render();
	
    
    }
    elseif($_SERVER['REQUEST_METHOD'] == 'GET' && (isset($_GET['remove'])))
    {
	$pageName = "list";
	$pageData = new RecipeModel;
	$pageData->removeRecipe($_GET['remove']);
	$pageData = $pageData->find_all();
	$viewObject = new View ($pageName, $pageData);
	$viewObject->Render();
	/*
	$recipe = new RecipeModel();
	$ID = $_GET["delete"];
	$recipe->Delete($ID);
	header('Location: index.php');
	*/
    }
	
    else
    {
	$pageName = "list";
	$pageData = new RecipeModel;
	$object = $pageData->find_all();
	$viewObject = new View($pageName, $object);
	$viewObject->Render();
    }
    



?>


 