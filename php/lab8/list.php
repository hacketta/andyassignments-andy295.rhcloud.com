         
<a href="index.php?action=insert">Insert a recipe</a>

<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>title</th>                  
            <th>ingrediant 0</th>
            <th>ingrediant 1</th>
            <th>ingrediant 2</th>
            <th>instructions</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($this->data as $recipe) { ?>     
        <tr>
            <td><?php echo htmlentities($recipe->id); ?></td>
            <td><?php echo htmlentities($recipe->title); ?></td>                                
            <td><?php echo htmlentities($recipe->ingrediant0); ?></td>
            <td><?php echo htmlentities($recipe->ingrediant1); ?></td>
            <td><?php echo htmlentities($recipe->ingrediant2); ?></td>
            <td><?php echo htmlentities($recipe->instructions); ?></td>
        </tr>                                
   <?php  } ?>          
    </tbody>                
</table> 