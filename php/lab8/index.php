       
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


require_once "conn.php";
require_once "recipe.php";
require_once "view.php";
require_once "userlogin.php";
    $view = NULL;
    session_start();
    
    if($_SERVER['REQUEST_METHOD'] == 'POST' && (isset($_GET['action']) && $_GET['action'] == 'login'))
    {
	if (success())
	{
	    $pageName = "list";
        $pageData = new RecipeModel;
        $object = $pageData->find_all();
        $viewObject = new View($pageName, $object);
        $viewObject->Render();
	} else {
	header('Location: loginview.php');
       }
 }
if(isset($_SESSION['usersname'])){
    if($_SERVER['REQUEST_METHOD'] == 'GET' && (isset($_GET['action']) && $_GET['action'] == 'insert'))
    {
        $pageName = "insert";
        $pageData = NULL;
        $viewObject = new View($pageName, $pageData);
        $viewObject->Render();
    }
    
    elseif($_SERVER['REQUEST_METHOD'] == 'POST' && (isset($_GET['action']) && $_GET['action'] == 'insert'))
    {
        $pageName = "thanks";
        $pageData = new RecipeModel;
        $pageData->insertRecipe();
        $viewObject = new View($pageName, $pageData);
        $viewObject->Render();    
    }
    
    else
    {
        $pageName = "list";
        $pageData = new RecipeModel;
        $object = $pageData->find_all();
        $viewObject = new View($pageName, $object);
        $viewObject->Render();
    }
}else{
     header('Location: loginview.php');
}
       
?>     