<?php
    require_once "conn.php";
    require_once "model.php";
    
    class Recipe
    {
        public $id;
        public $title;
        public $ingrediant0;
        public $ingrediant1;
        public $ingrediant2;
        public $instructions;
        
        function __construct($id, $title, $ingrediant0, $ingrediant1, $ingrediant2, $instructions){
            $this->id = $id;
            $this->title = $title;
            $this->ingrediant0 = $ingrediant0;
            $this->ingrediant1 = $ingrediant1;
            $this->ingrediant2 = $ingrediant2;
            $this->instructions = $instructions;
        }
    }
        
    class RecipeModel extends Model
    {    
        function find_all()
            {
              
                    $results = array();
                
                    $conn = Database::get_connection();
                    
                    $query = "SELECT * from recipies";
                    $res = $conn->query($query);
     
                    while ($row = $res->fetch_assoc())
                    {
                        $results[] = new Recipe(
                            $row{'id'},
                            $row{'title'},
                            $row{'ingrediant0'},
                            $row{'ingrediant1'},
                            $row{'ingrediant2'},
                            $row{'instructions'});
                    }           
                        
                        return $results;
             } 
         
    
                    function insertRecipe()
        {
            
                      $conn = Database::get_connection();
                    $title = htmlentities($_POST['title']);
                    $ingrediant0 = htmlentities($_POST['ingrediant0']);
                   $ingrediant1 = htmlentities($_POST['ingrediant1']);
                   $ingrediant2 = htmlentities($_POST['ingrediant2']);
                   $instructions = htmlentities($_POST['instructions']);
               
               
               
                $query = $conn->prepare("INSERT INTO recipies (title, ingrediant0, ingrediant1, ingrediant2, instructions) VALUES (?, ?, ?, ?, ?)");      
                $query->bind_param('sssss', $title, $ingrediant0, $ingrediant1, $ingrediant2, $instructions);
                $query->execute();
        }
    }
    
    ?>